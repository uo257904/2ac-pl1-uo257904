#include <stdio.h>
#include <string.h>

typedef struct _Person Person;
typedef Person* PPerson;

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};

int main(int argc, char* argv[])
{
    Person Peter;
    PPerson pPeter;

    // Memory location of Javier variable is assigned to the pointer
    pPeter = &Peter;
    Peter.heightcm = 175;
    Peter.weightkg = 78.7;
    (*pPeter).weightkg = 80.0;

    printf("Peter's height: %d", Peter.heightcm);
    printf("; ");
    printf("Peter's weight: %.1f\n", Peter.weightkg); 
    return 0;
}